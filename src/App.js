import React from 'react';
import { Formik, Form, Field, ErrorMessage, FieldArray } from 'formik';
import * as yup from 'yup';

yup.addMethod(yup.string, 'restrictedEmails', function (msg) {
  return this.test('restrictedEmails', msg, function (val) {
    if (!this.schema.isType(val)) return true;
    return !val.endsWith('.ru');
  });
});

const validator = yup.object().shape({
  email: yup
    .string()
    .email('Введите корректный email')
    .restrictedEmails('Email запрещен!')
    .required('Поле email не может быть пустым'),
  password: yup
    .string()
    .min(4, 'Поле рassword должено быть больше 4-х символов')
    .max(10, 'Поле рassword должено быть mеньше 10-ти символов')
    .required('Поле password не может быть пустым'),
  country: yup.string().required(),
  city: yup
    .string()
    .min(3, 'Поле рassword должено быть больше 3-х символов')
    .required(),
  skills: yup.array().required(),
});

class App extends React.Component {
  onSubmit = (values, { resetForm }) => {
    console.log('App -> onSubmit -> values', values);
    resetForm();
  };

  render() {
    return (
      <div className="container">
        <Formik
          onSubmit={this.onSubmit}
          validationSchema={validator}
          initialValues={{
            email: '',
            password: '',
            country: 'ua',
            city: 'Киев',
            skills: [],
          }}
        >
          {({ isValid, touched, errors, values, setFieldValue }) => {
            const isSubmitDisabled = !Object.keys(touched).length || !isValid;
            return (
              <Form className="card">
                <h1>
                  React <sub>formik/yup</sub> Forms
                </h1>

                <div className="form-control">
                  <label>Email</label>
                  <Field
                    placeholder="Email"
                    name="email"
                    type="text"
                    className={touched.email && errors.email ? 'invalid' : ''}
                  />
                  <ErrorMessage
                    name="email"
                    render={(msg) => <div>{msg}</div>}
                  />
                </div>

                <div className="form-control">
                  <label>Пароль</label>
                  <Field
                    placeholder="Пароль"
                    name="password"
                    type="password"
                    className={
                      touched.password && errors.password ? 'invalid' : ''
                    }
                  />
                  <ErrorMessage
                    name="password"
                    render={(msg) => <div>{msg}</div>}
                  />
                </div>

                <div className="card">
                  <h2>Адрес</h2>

                  <div className="form-control">
                    <label>Страна</label>

                    <Field
                      as="select"
                      name="country"
                      onChange={(ev) => {
                        const newCountry = ev.target.value;
                        const mapCity = {
                          ua: 'Киев',
                          pl: 'Варшава',
                          de: 'Берлин',
                        };
                        setFieldValue('country', newCountry);
                        setFieldValue('city', mapCity[newCountry]);
                      }}
                    >
                      <option value="ua">Украина</option>
                      <option value="pl">Польша</option>
                      <option value="de">Германия</option>
                    </Field>
                  </div>

                  <div className="form-control">
                    <Field
                      placeholder="Столица"
                      name="city"
                      type="text"
                      className={touched.city && errors.city ? 'invalid' : ''}
                    />
                  </div>
                  <button type="button" className="btn">
                    Выбрать столицу
                  </button>
                </div>

                <div className="card">
                  <h2>Ваши навыки</h2>
                  <FieldArray
                    name="skills"
                    render={(arrayHelpers) => (
                      <React.Fragment>
                        <button
                          type="button"
                          className="btn"
                          onClick={() => arrayHelpers.push('')}
                        >
                          Добавить умение
                        </button>
                        {values.skills.map((skill, index) => (
                          <div className="form-control" key={index}>
                            <label>Умение #{index + 1}</label>
                            <Field
                              placeholder="Skill"
                              name={`skills.${index}`}
                              type="text"
                              className={
                                touched.city && errors.city ? 'invalid' : ''
                              }
                            />
                            <button
                              type="button"
                              className="btn"
                              onClick={() => arrayHelpers.remove(index)}
                            >
                              Удалить #{index + 1}
                            </button>
                          </div>
                        ))}
                      </React.Fragment>
                    )}
                  />
                </div>

                <button
                  type="submit"
                  className="btn"
                  disabled={isSubmitDisabled}
                >
                  Отправить
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
    );
  }
}

export default App;
